package com.chessteam.chessteam.controller;

import com.chessteam.chessteam.model.ChessTeamModel;
import com.chessteam.chessteam.model.TournamentModel;
import com.chessteam.chessteam.repository.ChessTeamRepository;
import com.chessteam.chessteam.repository.TournamentRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class ChessTeamController {
    private ChessTeamRepository chessTeamRepository;
    private TournamentRepository tournamentRepository;
    public ChessTeamController(ChessTeamRepository chessTeamRepository, TournamentRepository tournamentRepository)
    {
        this.chessTeamRepository = chessTeamRepository;
        this.tournamentRepository = tournamentRepository;
    }

    @GetMapping("/players")
    public List<ChessTeamModel> getAllPlayers() {
        return (List<ChessTeamModel>)chessTeamRepository.findAll();
    }

    @GetMapping("/tournaments")
    public List<TournamentModel> getAllTournaments() {
        return (List<TournamentModel>)tournamentRepository.findAll();
    }

    @PostMapping("/create")
    public ChessTeamModel createPlayer(@RequestBody ChessTeamModel creatme) {

        chessTeamRepository.save(creatme);
        return creatme;
    }

    @PutMapping("/player/update")
    public List<ChessTeamModel> updateStudent(@RequestBody ChessTeamModel playerUpdate) {
        this.chessTeamRepository.save(playerUpdate);
        return getAllPlayers();
    }
    @GetMapping("/add/player/{id}")
    public Optional<ChessTeamModel> getPlayerById(@PathVariable Long id){
        return chessTeamRepository.findById(id);
    }
    @DeleteMapping("/player/delete/{id}")
    public List<ChessTeamModel>  deletePlayer(@PathVariable Long id){
        chessTeamRepository.deleteById(id);
        return this.getAllPlayers();
    }
}
