package com.chessteam.chessteam.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class ChessTeamModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Long id;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column (name ="rating")
    private Double rating;
    @Column(name = "wins")
    private Double wins;
    @Column(name = "losses")
    private Double losses;
    @Column(name = "gender")
    private String gender;
    @Column(name = "age")
    private String age;
    @Column(name = "grade")
    private String grade;
    @Column(name = "uscf")
    private Double uscf;
    @Column(name = "team")
    private String team;
}
