package com.chessteam.chessteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChessteamApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChessteamApplication.class, args);
    }

}
