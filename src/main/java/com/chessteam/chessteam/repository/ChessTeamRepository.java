package com.chessteam.chessteam.repository;

import com.chessteam.chessteam.model.ChessTeamModel;
import org.springframework.data.repository.CrudRepository;

public interface ChessTeamRepository extends CrudRepository <ChessTeamModel, Long> {
}
