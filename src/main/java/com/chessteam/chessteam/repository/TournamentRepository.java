package com.chessteam.chessteam.repository;

import com.chessteam.chessteam.model.TournamentModel;
import org.springframework.data.repository.CrudRepository;

public interface TournamentRepository extends CrudRepository <TournamentModel, Long> {
}
